﻿using Microsoft.EntityFrameworkCore;
using PPTBootCamp.Entities;

namespace PPTBootCamp
{
    public class BootCampContext : DbContext
    {
        public BootCampContext(DbContextOptions<BootCampContext> options)
        : base(options)
        {
            
        }

        public DbSet<Tutor> Tutors { get; set; }
        public DbSet<Course> Courses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BootCampContext).Assembly);
        }
    }
}