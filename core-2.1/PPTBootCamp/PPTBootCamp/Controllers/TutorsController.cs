﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using PPTBootCamp.Entities;
using PPTBootCamp.Models;

namespace PPTBootCamp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TutorsController : ControllerBase
    {
        public BootCampContext Context { get; }

        public TutorsController(BootCampContext context)
        {
            Context = context;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Tutor>> GetTutors()
        {
            return Ok(Context.Tutors.ToList());
        }

        // GET api/tutors/5
        [HttpGet("{id}", Name = "GetTutorById")]
        public ActionResult<Tutor> GetTutorById(int id)
        {
            Tutor tutor = Context.Tutors.Single(src => src.TutorId == id);

            if (tutor == null)
                return NotFound();

            return Ok(tutor);
        }

        // POST api/tutors
        [HttpPost]
        public IActionResult CreateTutor([FromBody] TutorForCreation tutorForCreation)
        {
            if (tutorForCreation == null)
                return BadRequest("Must submit a tutor. \n eg. {'firstName':'John',\n'lastName':'Doe'}");

            var newTutor = new Tutor
            {
                FirstName = tutorForCreation.FirstName,
                LastName = tutorForCreation.LastName,
                MiddleInitial = tutorForCreation.MiddleInitial
            };

            Context.Tutors.Add(newTutor);
            Context.SaveChanges();

            return CreatedAtRoute(
                "GetTutorById",
                new {id = newTutor.TutorId},
                newTutor);
        }

        // PUT api/tutors/5
        [HttpPut("{id}")]
        public IActionResult UpdateTutor(int id, [FromBody] object value)
        {
            return Ok();
        }

        // DELETE api/tutors/5
        [HttpDelete("{id}")]
        public IActionResult DeleteTutor(int id)
        {
            return Ok();
        }
    }
}
