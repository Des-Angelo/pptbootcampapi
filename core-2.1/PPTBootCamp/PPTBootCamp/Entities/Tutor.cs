﻿using System.Collections.Generic;

namespace PPTBootCamp.Entities
{
    public class Tutor
    {
        public int TutorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitial { get; set; }
        public string FullName => $"{FirstName} {MiddleInitial} {LastName}";
        public ICollection<Course> Courses { get; }

        public Tutor()
        {
            Courses = new HashSet<Course>();
        }
    }
}